<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header();
?>
    <div id="content" class="content-area" role="main">
	<?php
		$type = get_post_format();
        $posttype = get_post_type();
		if ( $posttype === 'post' || $posttype === 'event' ) {
			$type = 'single';
		}
		if ( $posttype === 'page' ) {
			$type = 'page';
		}
		get_template_part( 'content/content', $type );
		if ( $posttype !== 'event' && ( comments_open() || get_comments_number() ) ) {
			comments_template();
		}
	?>
	</div>
<?php get_footer(); ?>
