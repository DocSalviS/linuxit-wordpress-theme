<?php
$search_terms = '';
if ( isset( $_GET["s"] ) ) {
	$search_terms = htmlspecialchars( $_GET["s"] );
}
?>

<form role="form" action="<?php bloginfo('url'); ?>/" id="searchform" method="get">
    <label for="s" class="sr-only">Cerca</label>
    <div class="input-group">
        <input type="text" class="form-control" id="s" name="s" placeholder="Cerca"<?php if ( $search_terms !== '' ) { echo ' value="' . $search_terms . '"'; } ?> />
        <span class="input-group-btn">
            <button type="submit" class="btn btn-primary">Cerca</button>
        </span>
    </div>
</form>
