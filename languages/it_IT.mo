��          �   %   �      @     A  
   W     b     o     �     �     �     �     �     �  _   �  W   !     y     ~     �  %   �     �     �  F   �  \   #     �  U   �  .   �       x       �     �  
   �     �     �     �     	     	     	     	  e   0	  J   �	     �	     �	     
  ,   
     A
     I
  I   Y
  n   �
       $     !   @     b                                                                           
                              	                     &larr; Older Comments Add a menu Categories:  Comment navigation Comments are closed. Edit Edit %s Email Header Header settings It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Name Newer Comments &rarr; Nothing Found Oops! That page can&rsquo;t be found. Pages: Primary Menu Ready to publish your first post? <a href="%1$s">Get started here</a>. Sorry, but nothing matched your search terms. Please try again with some different keywords. Website comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; comments titleOne thought on &ldquo;%s&rdquo; nounComment Project-Id-Version: linuxit
PO-Revision-Date: 2021-08-19 12:32+0200
Last-Translator: mte90 <mte90net@gmail.com>
Language-Team: Italian <kde-i18n-it@kde.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Generator: Lokalize 21.04.3
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-Basepath: ..
Language: it_IT
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: js
X-Poedit-SearchPathExcluded-1: node_modules
X-Poedit-SearchPathExcluded-2: src
 &larr; Commenti meno recenti Aggiungi un menu Categorie: Navigazione commenti Commenti disabilitati. Modifica Modifica %s Email Testata Impostazioni testata Sembra che nulla sia stato trovato a questo indirizzo. Magari prova uno dei link sotto o una ricerca? Sembra che non troviamo quello che cerchi. Forse una ricerca può aiutare. Nome Commenti più recenti &rarr; Nessun Risultato Oops! Quella pagina non può essere trovata. Pagine: Menu principale Pronto a pubblicare il tuo primo articolo? <a href="%1$s">Inizia qui</a>. Spiacente, ma non ci sono riscontri con i tuoi termini di ricerca. Per favore riprova con altre parole chiavi. Sito Web %1$s commenti su &ldquo;%2$s&rdquo;  Un commento su &ldquo;%2$s&rdquo; Commento 