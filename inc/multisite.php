<?php

// Modificato per dare agli admin di ogni sito la possibilità di aggiungere css custom
add_filter( 'map_meta_cap', 'multisite_custom_css_map_meta_cap', 20, 3 );
function multisite_custom_css_map_meta_cap( $caps, $cap, $user_id ) {
	$user_meta = get_userdata($user_id);
	if ( $user_meta ) {
		$user_roles = $user_meta->roles;
		if ( in_array("administrator", $user_roles) ){
			$caps = array( 'edit_theme_options' );
		}
	}
	return $caps;
}

// WordPress MultiSite sends user signup mails from the main site. This is a problem when using domain mapping functionality as the sender is not the same domain as expected when creating a new user from a blog with another domain.
add_filter( 'wpmu_signup_user_notification', 'dk_wpmu_signup_user_notification', 10, 4 );
function dk_wpmu_signup_user_notification($user, $user_email, $key, $meta = '') {
	$blog_id = get_current_blog_id();
	// Send email with activation link.
	$admin_email = get_option( 'admin_email' );
	if ( $admin_email == '' )
		$admin_email = 'support@' . $_SERVER['SERVER_NAME'];
	$from_name = get_option( 'blogname' ) == '' ? 'WordPress' : esc_html( get_option( 'blogname' ) );
	$message_headers = "From: \"{$from_name}\" <{$admin_email}>\n" . "Content-Type: text/plain; charset=\"" . get_option('blog_charset') . "\"\n";
	$message = sprintf(
		apply_filters( 'wpmu_signup_user_notification_email',
			__( "To activate your user, please click the following link:\n\n%s\n\nAfter you activate, you will receive *another email* with your login.\n\n" ),
			$user, $user_email, $key, $meta
		),
		site_url( "wp-activate.php?key=$key" )
	);
	$subject = sprintf(
		apply_filters( 'wpmu_signup_user_notification_subject',
			__( '[%1$s] Activate %2$s' ),
			$user, $user_email, $key, $meta
		),
		$from_name,
		$user
	);
	wp_mail($user_email, $subject, $message, $message_headers);

	return false;
}
