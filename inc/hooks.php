<?php

add_filter( 'get_the_excerpt', 'replace_post_excerpt_filter' );

function replace_post_excerpt_filter($output) {
        $yoast = get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);
        if (empty($yoast)) {
			return $output;
        }
        return $yoast;
}

remove_action( 'wp_head', 'rsd_link' );// Windows Live Writer? Ew!
remove_action( 'wp_head', 'wlwmanifest_link' );// Windows Live Writer? Ew!
remove_action( 'wp_head', 'wp_generator' );// No need to know my WP version quite that easily
add_filter( 'xmlrpc_enabled', '__return_false' );
add_action( 'init', 'disable_emojis' );

function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}

add_filter( 'w3tc_minify_urls_for_minification_to_minify_filename', 'w3tc_filename_filter', 20, 3 );
function w3tc_filename_filter( $minify_filename, $files, $type ) {
	$path_parts = pathinfo( $minify_filename );
	$content    = '';
	foreach ( $files as &$asset ) {
		$content .= file_get_contents( ABSPATH . '/' . $asset );
	}

	return substr( md5( $content ), 10 ) . '.' . $path_parts[ 'extension' ];
}
